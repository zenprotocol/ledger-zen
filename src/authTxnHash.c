#include <stdbool.h>
#include <string.h>
#include <os_io_seproxyhal.h>

#include "ux.h"
#include "bech32.h"
#include "zen.h"
#include "retEnums.h"

#define testCase(p1,p2,p3,p4) testCaseIfed(p1,true,p2,p3,p4)
#define testCaseIfed(p1,p2,p3,p4,p5) if ((p1 == context.debugCase) && (p2)) {debugSend(0xdb, 0xdb, 0xdbdb, p3, p4, p5);}
#define testCaseReturned(p1,p2,p3,p4,p5) {}

//This function is used to debug with, it should never be used in production code
void debugSend(uint8_t p1, uint8_t p2, uint16_t p3, uint16_t p4, uint8_t* buffer, uint16_t bufferSize) {

    G_io_apdu_buffer[0] = p1;
    G_io_apdu_buffer[1] = p2;
    G_io_apdu_buffer[2] = (uint8_t)(p3 >> 8);
    G_io_apdu_buffer[3] = (uint8_t)(p3 & 0xFF);
    G_io_apdu_buffer[4] = (uint8_t)(p4 >> 8);
    G_io_apdu_buffer[5] = (uint8_t)(p4 & 0xFF);
    
    uint16_t index = 6;
    
    for (uint16_t secondIndex = 0; secondIndex < bufferSize; secondIndex++) {
        G_io_apdu_buffer[index++] = buffer[secondIndex];
    }

    G_io_apdu_buffer[index++] = 0x90;
    G_io_apdu_buffer[index++] = 0x00;

    io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX , index);
}

//Writes size zeros to buffer
void zeroOut(uint8_t * buffer, uint16_t size) {
    while (size-- > 0)
        buffer[size] = 0;
}


//This is the state or context struct used for transaction hashing an siging
//Each time we hash a txn we call initState() and it zero's it out
typedef struct {

    uint8_t debugCase;

    bool isEmpty; //At first this is true, after the first parse it will be false

    //The buffer we're parsing, it's big because the input buffer we get is at most 255 bytes in size
    //and sometimes we're parsing and we're lacking some extra data, in this case the buffer is already (<255) bytes
    //in size, and we're adding at most 255 to it, so we need 512 to handle the worst case.
    //probably most of the time all of these bytes arn't going to be utilized, so you can put in some work and optimize it
    //if needed
    uint8_t buffer[512];
    
    //The ptr in memory for the section of the buffer that hasn't been read yet
    uint8_t * bufferReadPtr;

    //The position in the buffer of the byte that hasn't been read yet
    uint16_t bufferReadOffset;

    //The position in the buffer of byte after the last one for the data thats been loaded
    uint16_t bufferEndPos;

    //Sometimes we try to parse the buffer for a certian complicated type, and we can't be sure there's enough bytes
    //to read the whole object, so we use uncommittedReadPtr to point to the place we are trying to read
    //if the read fails, the Ptr is reverted to bufferReadPtr
    uint8_t * uncommittedReadPtr;
    uint16_t uncommittedReadOffset; //In relation to uncommittedReadPtr, its like bufferEndPos to bufferReadOffset
    
    //VERY IMPORTANT!: The current position of the parser in the parsing process
    uint8_t gotoPos;

    //Bellow are all temp buffers used for parsing, they hold the data, while other parts are being parsed
    uint8_t tempBuffer[32];
    
    uint8_t assetContractSubType[32];
    uint8_t assetContractHash[32];
    uint32_t assetVersion;
    uint64_t assetAmount;
    uint64_t spendAmount;
    uint32_t numForLoop;
    uint32_t tempVar1;
    uint32_t tempVar2;
    uint8_t tempVar3;
    bool tempVar4;
    uint32_t tempVar5;
    uint32_t iterIndex;
    uint8_t tempStr1[78]; //should hold a zen address or a 32 bit val and hash in hex
    uint8_t tempStr2[146]; //will hold a "of asset " + 32*2*2+9 char idetifier

    //Strings used for the auth dialog
    uint8_t dialogTitle[64];
    uint8_t dialogContent[264]; //21 digits for the amount(8 bytes) + 82 token identifier + 146 zen address or version + hash + 5 extra text + 13 (for just in case)

    cx_sha3_t shaContext; //The context used by cx_hash for hashing
    uint8_t txHash[32]; //The final hash is held here

    bool isTxnAuthorized; //Is true iff the user autherized all the displayed outputs for the txn

    uint8_t debugCounter; //this is used for debuging purposes

} signTxnContext_t;

//The instance for the context object
signTxnContext_t context;

//This function clears out the state, as if we are starting to hash the txn from scratch
void initState() {

    context.debugCase = 0;

    context.debugCounter = 0;

    context.isEmpty = true;
    
    context.isTxnAuthorized = false;
    zeroOut(context.txHash, 32);
    cx_sha3_init(&context.shaContext, 256);

    context.bufferReadPtr = context.buffer;
    context.bufferReadOffset = 0;
    context.bufferEndPos = 0;
    context.uncommittedReadPtr = context.buffer;
    context.uncommittedReadOffset = 0;

    context.assetVersion = 0;
    context.assetAmount = 0;
    context.spendAmount = 0;

    context.gotoPos = 0;
    context.numForLoop = 0;
    context.tempVar1 = 0;
    context.tempVar2 = 0;
    context.tempVar3 = 0;
    context.tempVar4 = false;
    context.tempVar5 = 0;
    context.iterIndex = 0;

    zeroOut(context.buffer, sizeof(context.buffer));
    zeroOut(context.tempBuffer, sizeof(context.tempBuffer));
    zeroOut(context.assetContractSubType, sizeof(context.assetContractSubType));
    zeroOut(context.assetContractHash, sizeof(context.assetContractHash));


    zeroOut(context.dialogTitle, sizeof(context.dialogTitle));
    zeroOut(context.dialogContent, sizeof(context.dialogContent));

    zeroOut(context.tempStr1, sizeof(context.tempStr1));
    zeroOut(context.tempStr2, sizeof(context.tempStr2));
}



//This function adds data to context.buffer once new data is recieved
//whenever something is added to the buffer, it's hashed as you go along
//returns: True iff there is enough space in the buffer for the new data

// TODO: Further documentation here
bool addToBuffer(uint8_t* buffer, uint16_t size) {

    if (context.bufferEndPos - context.bufferReadOffset + size > 512)
        return false; //This shouln't really happend, but ok

    uint16_t index = 0;

    while (context.bufferReadOffset < context.bufferEndPos)
        context.buffer[index++] = context.buffer[context.bufferReadOffset++];

    context.bufferEndPos = index;
    context.bufferReadOffset = 0;

    index = 0;

    while (index < size)
        context.buffer[context.bufferEndPos++] = buffer[index++];

    context.bufferReadPtr = context.buffer;

    cx_hash(&context.shaContext.header, 0, buffer, size, context.txHash, sizeof(context.txHash));

    return true;
}

// This is the button handler for the output approval screen
// If REJECT is pressed then the state is reset, so that the transaction will never reach an approved state
// and therefore never signed. If it is accepted, it is expected that the comunicating device will
// send an "empty packet" (just the auth key) to the ledger in order to reinitated the parseBuffer() function, 
// so that the parsing would continue
static unsigned int ui_auth_txt_button(unsigned int button_mask, unsigned int button_mask_counter) {

    switch (button_mask) {
        case BUTTON_EVT_RELEASED | BUTTON_LEFT: // REJECT
            initState();
            G_io_apdu_buffer[0] = RET_VAL_REJECT_BUTTON;
            G_io_apdu_buffer[1] = 0x90;
            G_io_apdu_buffer[2] = 0x00;
            io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX , 3);
            ui_idle();
            break;

        case BUTTON_EVT_RELEASED | BUTTON_RIGHT: // APPROVE
            G_io_apdu_buffer[0] = RET_VAL_ACCEPT_BUTTON;
            G_io_apdu_buffer[1] = 0x90;
            G_io_apdu_buffer[2] = 0x00;
            io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 3);
            ui_idle();
            break;
    }

    return 0;
}


//This is the aproval screen for a specific output
static const bagl_element_t ui_auth_txt[] = {
        UI_BACKGROUND(),
        {{BAGL_ICON,0x00,3,4,7,7,0,0,0,0xFFFFFF,0,0,BAGL_GLYPH_ICON_CROSS},NULL,0,0,0,NULL,NULL,NULL},
        {{BAGL_ICON,0x00,117,4,8,6,0,0,0,0xFFFFFF,0,0,BAGL_GLYPH_ICON_CHECK},NULL,0,0,0,NULL,NULL,NULL},
        UI_TEXT(0x00, 0, 12, 128, context.dialogTitle), //The title
        //The next line initiates a rotating text
        {{BAGL_LABELINE,0x01,12,26,104,12,10,0,0,0xFFFFFF,0,BAGL_FONT_OPEN_SANS_REGULAR_11px|BAGL_FONT_ALIGNMENT_CENTER,26},(char*)context.dialogContent,0,0,0,NULL,NULL,NULL}
};

//Needed to make the second line rotate thing work
unsigned int ui_auth_txt_prepro(const bagl_element_t *element) {
    if (element->component.userid > 0) {
        UX_CALLBACK_SET_INTERVAL(MAX(
            3000, 1000 + bagl_label_roundtrip_duration_ms(element, 7)));
    }
    return 1;
}

//Since we try to parse uncommitted bytes sometimes we fail cuz we need more data
//This is the function that is called to reverse the state
void resetUncommitted() {
    context.uncommittedReadPtr = context.bufferReadPtr;
    context.uncommittedReadOffset = context.bufferReadOffset;
}

/*  Notes: Say you want to read numBytes bytes from the uncommitted location, and move the uncommitted 
    ptr numBytes forward, also you don't know if there is enough bytes left in the buffer, then what you do is this:

    bool func() {
        uint8_t * readPtr = moveUncommitted(numBytes);

        if (0 == readPtr)
            return false;

        //Some code that enjoys numBytes from readPtr
    }

    uint_8 parseBuffer() {
        //code

        if (!commitUncommittedRead(func()))
            return PARSE_BUFFER_RET_NEED_MORE_BUFFER;

        //more code
    }

    
    Returns: If there aren't enough bytes in the buffer (from the uncommitted location) then moveUncommitted returns 0 ->
    func returns false -> commitUncommittedRead reverts the uncommitted buffer to the committed location and parseBuffer returns 
    PARSE_BUFFER_RET_NEED_MORE_BUFFER signaling the calling client to send more bytes.

    If there are enough bytes in the buffer (from the uncommitted location) then moveUncommitted returns the current uncommittedBufferPtr
    location, and moves the uncommittedBufferPtr numBytes forward, func will return true and then commitUncommittedRead will commit
    the uncommittedBufferPtr and return true, parseBuffer will continue parsing the next step
*/
uint8_t * moveUncommitted(uint16_t numBytes) {

    if (context.uncommittedReadOffset + numBytes > context.bufferEndPos)
        return 0;


    context.uncommittedReadPtr += numBytes;
    context.uncommittedReadOffset += numBytes;

    return context.uncommittedReadPtr - numBytes;
}

/* Currently not in use
uint8_t * moveBackUncommitted(uint8_t numBytes) {

    if (context.uncommittedReadOffset < numBytes)
        return 0;


    context.uncommittedReadPtr -= numBytes;
    context.uncommittedReadOffset -= numBytes;

    //checking if we went beyond the committed marker
    if (context.uncommittedReadOffset < context.bufferReadOffset)
        return 0;

    return context.uncommittedReadPtr;}
*/
bool commitUncommittedRead(bool inp) {
    if (!inp)
        return false;

    context.bufferReadPtr = context.uncommittedReadPtr;
    context.bufferReadOffset = context.uncommittedReadOffset;

    return true;
}

//Note: This function reads into the numbytes bytes into the buffer in reverse (in order to convert from BE (Zen transactions)
//into LE (The ledger Nano format). It also does so uncommittedly, meaning it only moves the uncommitted ptr forward after reading
//Returns: true iff there are at least numBytes left in the buffer from the uncommitted location.
bool readIntoBufferReversedUncommitted(uint8_t * buffer, uint16_t numBytes) {
    
    uint8_t * readPtr = moveUncommitted(numBytes);

    if (0 == readPtr)
        return false;

    uint16_t targetIndex = numBytes;
    uint16_t readIndex = 0;

    while (readIndex < numBytes)
        *(buffer + --targetIndex) = *(readPtr + readIndex++);

    return true;
}

//Same as above (readIntoBufferReversedUncommitted), but not in reverse
bool readIntoBufferUncommitted(uint8_t * buffer, uint16_t numBytes) {
    
    uint8_t * readPtr = moveUncommitted(numBytes);

    if (0 == readPtr)
        return false;

    os_memmove(buffer, readPtr, numBytes);

    return true;
}

//Returns: true iff it managed to read a varint (zen serilization type) from the buffer in a uncommitted way
bool readVarIntUncommitted(uint32_t * output) {
    *output = 0;
    uint8_t * readPtr;

    while(true) {

        if (!(readPtr = moveUncommitted(1)))
            return false;

        uint8_t read = *readPtr;

        *output = (*output << 7) | (read & 0x7F);
        if (read & 0x80){
            (*output)++;
        } else {
            return true;
        }
    }
}

//Reads from a specified buffer with bufferLeft bytes a var int into output
//Returns: 0 if there aren't enough bytes in the buffer, otherwize it returns the amount of bytes used to read
uint8_t readVarIntFromBuffer(uint32_t * output, uint8_t * buffer, uint32_t bufferLeft) {
    *output = 0;
    uint8_t bufferIndex = 0;

    while(true) {
        if (bufferIndex >= bufferLeft)
            return 0;

        uint8_t read = buffer[bufferIndex++];

        *output = (*output << 7) | (read & 0x7F);
        if (read & 0x80){
            (*output)++;
        } else {
            return bufferIndex;
        }
    }
}

//Reads an uncommitted byte of the buffer
bool readByteUncommitted(uint8_t *ret) {
    uint8_t* readPtr = moveUncommitted(1);
    if (0 == readPtr)
        return false;

    *ret = *readPtr;

    return true;
}

//Returns: true iff it managed to read an amount (zen serilization type) from the buffer in a uncommitted way
//This is a copy from the F# implementation of this type deserilization
//Out: outIsLegitSerialization iff neither NaN, Inf, nor non-canonical
bool readAmount(uint64_t * output, bool * outIsLegitSerialization) {

    *outIsLegitSerialization = true;
    uint8_t * readPtr = moveUncommitted(1);

    if (0 == readPtr)
        return false;

    uint8_t readValue = *readPtr;

    if ((0x7C == (readValue & 0x7E)) || (0x78 == (readValue & 0x7C))) { //inf and nan
        *outIsLegitSerialization = false;
        return true;
    } else if (0x7E == (readValue & 0x7E)) {
        if (0x80 <= readValue) {
            bool ret = readIntoBufferReversedUncommitted((uint8_t*)output, 8); // 72 bit
            //testCaseIfed(1, true, 0, output, 8); //txn 4 - output 17
            return ret;
        } else {    // 64 bit / 8 byte
            zeroOut((uint8_t*)output + 7, 1);
            bool ret = readIntoBufferReversedUncommitted((uint8_t*)output, 7);
            //testCaseIfed(2, true, 0, output, 8); //txn 4 - output 16
            return ret;
        }

    } else if (0x80 > readValue) { // MSB = 0
        if (0 == (readPtr = moveUncommitted(1)))
            return false;

        uint8_t readValue2 = *readPtr;
        uint8_t pow = 0;

        if (0x60 == (readValue & 0x60)) {   //inf and nan are already caught -- non-canonical representation
            outIsLegitSerialization = false;
            return true;
        } else {
            pow = (readValue & 0x7C) >> 2;
            (*output) = readValue2 + ((readValue & 0x03) << 8); // two bytes, canonical
            //testCaseIfed(4, true, 0, output, 8); //txn 4 - output 0
        }

        for (; pow > 0; pow--)
            (*output) *= 10;

        return true;
    }
    // MSB must now be 1
    if (0 == (readPtr = moveUncommitted(1)))
        return false;

    uint8_t second = *readPtr;
    uint16_t lower = 0;
    uint8_t pow = 0;

    if (!readIntoBufferReversedUncommitted((uint8_t*)&lower, 2))
        return false;

    if (0 == (readValue & 0x40)) {
        pow = (readValue & 0x3C) >> 2;
        (*output) = lower + (second << 16) + ((readValue & 0x03) << 24);
        //testCaseIfed(5, true, 0, output, 8); //txn 4 - output 4 
    } else {
        pow = (readValue & 0x1E) >> 1;
        (*output) = ((uint64_t)lower) + (((uint64_t)(second)) << 16) + (((uint64_t)(readValue & 0x01)) << 24) + 0x4000000;
        //testCaseIfed(6, true, 0, output, 8); //txn 4 - output 13 - 2^26 + 1
    }
    
    for (; pow > 0; pow--)
        (*output) *= 10;

    return true;
}


//Returns true iff it managed to read an asset type (zen protocol type) from the buffer in an uncommitted way
bool readAssetUncommitted(uint8_t * retContractHash32Bytes, uint8_t * retContractSubtype, uint32_t * retVersion) {
    
    uint8_t * readPtr = moveUncommitted(1);
    if (0 == readPtr)
        return false;

    uint8_t firstByteRead = *readPtr;

    //First we parse the version
    if (0 == (firstByteRead & 0b00100000)) {
        *retVersion = firstByteRead & 0b00011111;
    } else {
        *retVersion = firstByteRead & 0b00011111;

        for (int i = 0; i < 4; i++) {
            if (!(readPtr = moveUncommitted(1)))
                return false;
            
            uint8_t newRead = *readPtr;

            (*retVersion) <<= 7;
            (*retVersion) += newRead & 0b01111111;

            if (0 == (newRead & 0b10000000)) {
                break;
            }
        }
    }

    zeroOut(retContractHash32Bytes, 32);
    zeroOut(retContractSubtype, 32);

    //Not ZP case
    if (0 != (firstByteRead & 0b11000000)) {
        //testCaseIfed(10, true, 0, 0, 0);
        if (!readIntoBufferUncommitted(retContractHash32Bytes, 32))
            return false;
    }

    if (0b11000000 == (firstByteRead & 0b11000000)) {
        //testCaseIfed(11, true, 0, 0, 0);

        if (!readIntoBufferUncommitted(retContractSubtype, 32))
            return false;
    } else if (0b01000000 == (firstByteRead & 0b11000000)) {
        //testCaseIfed(12, true, 0, 0, 0);

        uint8_t * readPtr = moveUncommitted(1);
        if (0 == readPtr)
            return false;

        uint8_t numRead = *readPtr;

        if (!readIntoBufferUncommitted(retContractSubtype, numRead))
            return false;
    }

    return true;
}

//Moves the bufferReadPtr amount bytes forward (if there are enough), and reset the committed pointers
//returns: true iff there are enough bytes in the buffer
bool moveBufferPos(uint16_t amount) {

    if (context.uncommittedReadOffset + amount > context.bufferEndPos)
        return false;

    context.bufferReadOffset += amount;
    context.bufferReadPtr += amount;

    resetUncommitted();

    return true;
}

//This formats a zen amount into a string
//ZP amounts (isFloat = true) are in floats, meaning inp / 100 000 000
//Zen assets are integers (isFloat = false)

bool amountToString(uint8_t * strOut, uint16_t maxOutLen, uint64_t inp, bool isFloat) {
    uint8_t divIndex = 0;

    uint16_t writeIndex = 0;
    bool isWritingNums = false;
    bool isAddedDot = false;

    while (true) {
        divIndex++;

        uint8_t left = inp % 10;
        inp -= left;
        inp /= 10;

        if (0 != left)
            isWritingNums = true;

        if (9 == divIndex) {
            if (isWritingNums && (!isAddedDot) && (isFloat)) {
                isAddedDot = true;
                strOut[writeIndex++] = '.';
            }

            isWritingNums = true;
        }

        if (isWritingNums || (!isFloat))
            strOut[writeIndex++] = '0' + left;

        if (writeIndex >= maxOutLen)
            return false;

        if ((0 == inp) && ((9 <= divIndex) || (!isFloat)))
            break;
    }

    //reverse the string, cuz numbers are right to left but string left to right
    for (uint16_t i = 0; i < writeIndex - 1 - i; i++) {
        uint8_t temp = strOut[i];
        strOut[i] = strOut[writeIndex - i - 1];
        strOut[writeIndex - i - 1] = temp;
    }

    strOut[writeIndex] = 0;
    return true;
}

//Copies memory in reverse
void reverseMemcopy(uint8_t * dest, uint8_t * src, uint16_t size) {
    for (uint16_t index = 0; index < size; index++) {
        dest[index] = src[size - index - 1];
    }
}

//This function turns a contract ID -> varint + 32 bytes hash into a bech32 contract id
//OutStr size should be at least 71 bytes
uint8_t contractIdToBech32(uint8_t * inpBuffer, uint8_t inpBufferSize, uint8_t * outStr) {

    uint32_t contractVersion;
    uint8_t tempBuffer[32 + 4];

    uint8_t bytesUsed = readVarIntFromBuffer(&contractVersion, inpBuffer, inpBufferSize);
    if (0 == bytesUsed)
        return RET_VAL_PARSE_BUFFER_NOT_ENOUGH_BYTES_INNER_BUFFER;

    if (32 != inpBufferSize - bytesUsed)
        return RET_VAL_PARSE_BUFFER_HASH_TOO_SHORT;

    reverseMemcopy(tempBuffer, (uint8_t*)&contractVersion, 4);
    os_memmove(tempBuffer + 4, inpBuffer + bytesUsed, 32);

    if (0 == bech32Encode((char*)outStr, "czen", 0, tempBuffer, 36))
        return RET_VAL_PARSE_BUFFER_BECH32_ERR;

    return RET_VAL_SUCCESS;
}

//This is the main parsing function, it's called by the command implemented,
//it works by holding most temp varibles in the context and maintaining a gotoPos in the context.
//this allows to cool feature of exiting and the being called back again into the original state it was before it exited.
//Cool, right?
//We need this because were implementing streaming deserilization to read the transaction, and we never know if we have
//enough bytes to get the end, so whenever we ran out of bytes, we exit, then send a command for the cleint to send more bytes
//fill up the buffer with the new data and continue parsing with parseBuffer()
//This idea was original though up by Asher.M, implemented by Me. (He liked my other idea of using stacks more, but I liked goto better)

uint8_t parseBuffer() {

    resetUncommitted();

    switch (context.gotoPos) {
        case 0: break;
        case 1: goto ONE;
        case 2: goto TWO;
        case 3: goto THREE;
        case 4: goto FOUR;
        case 5: goto FIVE;
        case 6: goto SIX;
        case 7: goto SEVEN;
        case 8: goto EIGHT;
        case 9: goto NINE;
        case 10: goto TEN;
        case 11: goto ELEVEN;
        case 12: goto TWELVE;
        case 13: goto THIRTEEN;
        case 14: goto FOURTEEN;
        case 15: goto FIFTEEN;
        default: return RET_VAL_PARSE_BUFFER_GOTO_ERROR;
    }

    uint32_t version = 0;

    if (!commitUncommittedRead(readIntoBufferUncommitted((uint8_t*)&version, 4))) //Read the version
        return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;

    if (0 != version) //Make sure the version is 0x00
        return RET_VAL_PARSE_BUFFER_BAD_VERSION_ERR;

    ONE: context.gotoPos = 1;

    if (!commitUncommittedRead(readVarIntUncommitted(&context.numForLoop))) //Read the amount of inputs
        return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;

    for (;context.numForLoop > 0; context.numForLoop--) { //Loop for each input

        TWO: context.gotoPos = 2;
        if (!commitUncommittedRead(readByteUncommitted(&context.tempVar3))) //Read the input type
            return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;

        if (0x1 != context.tempVar3) { //We only support 0x01 input types (PK locks)
            return RET_VAL_PARSE_BUFFER_UNSUPPORTED_INPUT_ERR;
        }

        THREE: context.gotoPos = 3;
        if (!moveBufferPos(32)) //Read the transaction hash
            return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;

        FOUR: context.gotoPos = 4;
        if (!commitUncommittedRead(readVarIntUncommitted(&context.tempVar2))) //Read the output index
            return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;
    }

    FIVE: context.gotoPos = 5;
    if (!commitUncommittedRead(readVarIntUncommitted(&context.numForLoop))) //Read the amount of outputs 
        return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;


    if (0 == context.numForLoop)
        return RET_VAL_PARSE_BUFFER_ZERO_OUTPUTS_ERR;

    context.iterIndex = 0;

    SIX: context.gotoPos = 6;

    for (; context.iterIndex < context.numForLoop;) { //Loop for each output

        context.iterIndex++; //Can't be in the for loop header because we have to incremenet every time we come from the SIX goto

        snprintf((char*)context.dialogTitle, sizeof(context.dialogTitle), "Confirm %u/%u: ", context.iterIndex, context.numForLoop);

        SEVEN: context.gotoPos = 7;

        if (!commitUncommittedRead(readVarIntUncommitted(&context.tempVar1))) //Read the lock type
            return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;

        EIGHT: context.gotoPos = 8;
        if (!commitUncommittedRead(readVarIntUncommitted(&context.tempVar2))) //Read the size of the remainder of the lock
            return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;

        NINE: context.gotoPos = 9;
        if (!commitUncommittedRead(readIntoBufferUncommitted(context.tempBuffer, context.tempVar2))) //Read the remaining bytes of the lock
            return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;

        //Switch on the lock type
        switch(context.tempVar1) {
            case 0x1: //fee lock
                snprintf((char*)(context.dialogTitle) + strlen((char*)context.dialogTitle), sizeof(context.dialogTitle) - strlen((char*)context.dialogTitle), "Fee");
                break;
            case 0x2: //pklock
                if (32 != context.tempVar2)
                    return RET_VAL_PARSE_BUFFER_BAD_BUFFER_SIZE_ERR; // Throw some sort of error here, its supposed to be 32 bytes sha256 hash

                if (0 == bech32Encode((char*)context.tempStr1, "zen", 0, context.tempBuffer, context.tempVar2))
                    return RET_VAL_PARSE_BUFFER_BECH32_ERR;

                break;

            case 0x3: //activation sacrifice lock
                snprintf((char*)(context.dialogTitle + strlen((char*)context.dialogTitle)), sizeof(context.dialogTitle) - strlen((char*)context.dialogTitle), "Sac.");
                break;
            case 0x4: //send to contract lock

                { //for retVal def
                    uint8_t retVal = contractIdToBech32(context.tempBuffer, context.tempVar2, context.tempStr1);

                    if (RET_VAL_SUCCESS != retVal)
                        return retVal;
                }

                break;
            case 0x5: //extention sacrifice lock
                snprintf((char*)(context.dialogTitle + strlen((char*)context.dialogTitle)), sizeof(context.dialogTitle) - strlen((char*)context.dialogTitle), "ExSac.");
                
                { //for retVal def
                    uint8_t retVal = contractIdToBech32(context.tempBuffer, context.tempVar2, context.tempStr1);

                    if (RET_VAL_SUCCESS != retVal)
                        return retVal;
                }

                break;

            case 0x6: //coinbase lock - not now
                return RET_VAL_PARSE_BUFFER_UNSUPPORTED_OUTPUT_ERR; //QA: checked
            
            case 0x7: //destry lock
                snprintf((char*)(context.dialogTitle + strlen((char*)context.dialogTitle)), sizeof(context.dialogTitle) - strlen((char*)context.dialogTitle), "Dstry");
                break;

            default: // ?? lock
                return RET_VAL_PARSE_BUFFER_UNKNOWN_LOCK_ERR;
        }

        TEN: context.gotoPos = 10;
        //Read the asset type of the spend
        if (!commitUncommittedRead(readAssetUncommitted(context.assetContractHash, context.assetContractSubType, &(context.assetVersion))))
            return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;

        context.tempVar4 = true; //Is asset ZP?

        if (0 == context.assetVersion) {
            for (uint8_t i = 0; i < 32; i++) {
                if (context.assetContractHash[i] | context.assetContractSubType[i]) {
                    context.tempVar4 = false;
                    break;
                }
            }
        } else {
            context.tempVar4 = false;
        }
    
        if (context.tempVar4) {
            snprintf((char*)context.tempStr2, sizeof(context.tempStr2), "ZP");
        } else {

            snprintf((char*)context.tempStr2, sizeof(context.tempStr2), "of asset %08X", context.assetVersion);
            /* todo: fix this
            uint8_t endPos = strlen(context.tempStr2);


            for (uint8_t i = 0; i < 32; i++) {
                snprintf((char*)context.tempStr2 + endPos, 3, "%02X", *(context.assetContractHash + i));
                endPos += 2;
            }

            for (uint8_t i = 0; i < 32; i++) {
                snprintf((char*)context.tempStr2 + endPos, 3, "%02X", *(context.assetContractSubType + i));
                endPos += 2;
            }*/
        }

        ELEVEN: context.gotoPos = 11;
        bool isLegit; // some amounts have a non-canonical serialization. We don't generate or sign txs with such serializations
        
        if (!commitUncommittedRead(readAmount(&context.spendAmount, &isLegit))) //Read the spend amount
            return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;        

        if (!isLegit)
            return REV_VAL_PARSE_BUFFER_ILLEGAL_SERIALIZATION;

        context.gotoPos = 6; //We want to go back into the for loop after UX_DISPLAY

        context.dialogContent[0] = ' '; //Ledger magic, the start of a auto rotating string needs to be ' '

        if (!amountToString(context.dialogContent + 1, sizeof(context.dialogContent) - 1, context.spendAmount, context.tempVar4))
            return RET_VAL_PARSE_BUFFER_AMOUNT_TO_STRING_ERR;
        

        snprintf((char*)(context.dialogContent + strlen((char*)context.dialogContent)), sizeof(context.dialogContent) - strlen((char*)context.dialogContent), " %s", context.tempStr2);

        if ((0x02 == context.tempVar1) || (0x04 == context.tempVar1) || (0x05 == context.tempVar1))          
            snprintf((char*)(context.dialogContent + strlen((char*)context.dialogContent)), sizeof(context.dialogContent) - strlen((char*)context.dialogContent), " to %s", context.tempStr1);

        uint16_t dialogContentLength = strlen((char*)context.dialogContent);

        context.dialogContent[dialogContentLength] = ' '; //Ledger magic, the ending needs to be ' '
        context.dialogContent[dialogContentLength + 1] = 0; //Null terminator

        UX_DISPLAY(ui_auth_txt, (bagl_element_callback_t)ui_auth_txt_prepro);
        return RET_VAL_PARSE_BUFFER_DISPLAY;
    }

    TWELVE: context.gotoPos = 12;
    if (!commitUncommittedRead(readByteUncommitted(&context.tempVar3))) //Read the byte indicating if there is a contract to activate or not
        return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;

    if ((0 != context.tempVar3) && (1 != context.tempVar3))
        return RET_VAL_PARSE_BUFFER_PROTOCOL_ERR; //the var int is more then 1 byte long

    if (1 == context.tempVar3) {

        THIRTEEN: context.gotoPos = 13;
        if (!commitUncommittedRead(readVarIntUncommitted(&context.tempVar1))) //Read the contract version
            return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;

        FOURTEEN: context.gotoPos = 14;
        if (!commitUncommittedRead(readVarIntUncommitted(&context.tempVar1))) //Read the contract size
            return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;

        FIFTEEN: context.gotoPos = 15; //Read size bytes without doing anything with them
        while (context.tempVar1 > 0) {
            if (!moveBufferPos(1))
                return RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES;
            context.tempVar1--;
        }
    }

    if (context.bufferReadOffset != context.bufferEndPos) //Make sure we read all of the bytes, otherwise, there are going to be some bytes in the hash that arn't really used in the txn
        return RET_VAL_PARSE_BUFFER_NOT_ALL_BYTES_CONSUMED;

    cx_hash(&context.shaContext.header, CX_LAST, 0, 0, context.txHash, sizeof(context.txHash));
    context.isTxnAuthorized = true;

    
    return RET_VAL_PARSE_BUFFER_FINISHED;
}

//This function implements the final signing of an already authorized txn hash
//IN @databuffer: a ptr to the part of the command that has the params the set the singing key
//4 bytes for key index, then 4 bytes for coin type (change wallet and such), and 4 bytes for account number, all in Big Endian
//returns: 0 iff success, 1 for deriveKeyError, 2 for ecdsa sign error
uint8_t signHash(uint8_t * dataBuffer, uint8_t * destBuffer, uint8_t * outSize, uint16_t * outException) {

    uint32_t index, coinType, accountNumber;

    os_memmove(&index, dataBuffer, sizeof(index));
    os_memmove(&coinType, dataBuffer + sizeof(index), sizeof(coinType));
    os_memmove(&accountNumber, dataBuffer + sizeof(index) + sizeof(coinType), sizeof(accountNumber));

    cx_ecfp_private_key_t privateKey;

    if (!deriveZenKeypair(index, coinType, accountNumber, &privateKey, NULL, outException))
        return 1;

    unsigned int info = 0;

    BEGIN_TRY {
        TRY {
            uint8_t ret = cx_ecdsa_sign((cx_ecfp_private_key_t WIDE *)&privateKey, CX_LAST | CX_RND_RFC6979, 
                    CX_SHA256, context.txHash, 32, destBuffer, sizeof(G_io_apdu_buffer), &info);
            
            *outSize += ret;
        } 
        CATCH_OTHER(e) {
            *outException = e;
            return 2;     
        }
        FINALLY {
        }
    }
    END_TRY;

    return 0;
}

//Therse are the command that this handler handles, they are pass via the p1 param
#define INIT_COMMAND 0          //First initialization
#define CONTINUE_COMMAND 1      //continue, sending extra bytes to the buffer some times there are no extra
                                //bytes, when we ask for auth using the display and the auth is accepted
                                //we expect the client to send a continue command with no extra bytes to
                                //continue the processing
#define SIGN_COMMAND 2          //Signing the already autherized txn hash

//This is the function process the incoming command
void authTxnHashHandlerHelper(uint8_t p1, uint8_t p2, uint8_t *dataBuffer, uint16_t dataLength,
                volatile unsigned int *flags, volatile unsigned int *tx) {

    //Check if we are authorized, if not revert the state in order to be safe
    if (!globalAuthContext.isAuthenticated) {
        initState();
        G_io_apdu_buffer[(*tx)++] = RET_VAL_NOT_AUTHENTICATED_ERR;
        return;
    }

    //we should have at least AUTH_KEY_LENGTH of data, in case of error we revert the state
    if (dataLength < sizeof(globalAuthContext.authKey)) {
        initState();
        G_io_apdu_buffer[(*tx)++] = RET_VAL_BAD_SIZE_ERR;    
        return;
    }

    //make sure the auth key in the command is legit
    for (uint8_t i = 0; i < sizeof(globalAuthContext.authKey); i++) {
        if (dataBuffer[i] != globalAuthContext.authKey[i]) {
            initState();
            G_io_apdu_buffer[(*tx)++] = RET_VAL_BAD_AUTH_KEY_ERR;
            return;
        }
    }

    if ((CONTINUE_COMMAND == p1) || (INIT_COMMAND == p1)) {

        if (INIT_COMMAND == p1) {
            initState();

        } else {

            if (context.isEmpty) { //This is the case the we don't get the init command but the state is in it's initial state
                initState();
                G_io_apdu_buffer[(*tx)++] = RET_VAL_CONTINUE_CMD_BUT_STATE_IN_INIT_ERR;
                return;
            }

            if (context.isTxnAuthorized) {
                initState();
                G_io_apdu_buffer[(*tx)++] = RET_VAL_PARSE_BUFFER_NOT_ALL_BYTES_CONSUMED;
                return;
            }
        }

        context.isEmpty = false;

        //This adds the new bytes into our buffer
        if (!addToBuffer(dataBuffer + 32, dataLength - 32)) {
            G_io_apdu_buffer[(*tx)++] = RET_VAL_ADD_TO_BUFFER_ERR;
            return;
        }

        //This is sometimes used by debugging function, this turns on a specific breakpoint
        //We don't ifdef this out, since it can't be used to create an attack, it's just a byte copy...
        context.debugCase = p2;

        //Start or continue parsing the buffer
        uint8_t ret = parseBuffer();

        if (!((RET_VAL_PARSE_BUFFER_NEED_MORE_BYTES == ret) || (RET_VAL_PARSE_BUFFER_FINISHED == ret) || 
            (RET_VAL_PARSE_BUFFER_DISPLAY == ret)))
            initState(); //if it's not one of there legit return values, we have an error, so we need
                         //to clear state to remain safe


        //If parseBuffer decides it wants to display a authentication request msg it sets the flag to IO_ASYNCH_REPLY
        //Meaning, don't send anything back to the client, io_exchange in the main loop will block, 
        //and in some ledger magical way the button handler will be invoked when a button is pressed
        //authTxnHashHandler wont add 0x9000 at the end if IO_ASYNCH_REPLY flag is lit
        if (RET_VAL_PARSE_BUFFER_DISPLAY == ret) {
            *flags |= IO_ASYNCH_REPLY;
        } else {
            G_io_apdu_buffer[(*tx)++] = 0;
            G_io_apdu_buffer[(*tx)++] = ret;
            
            if (RET_VAL_PARSE_BUFFER_FINISHED == ret) {
                os_memmove(G_io_apdu_buffer + (*tx), context.txHash, 32);
                (*tx) += 32; 
            }
        }
    } else if (SIGN_COMMAND == p1) {

        if (!context.isTxnAuthorized) {
            G_io_apdu_buffer[(*tx)++] = RET_VAL_TXN_NOT_AUTHORIZED_ERR;
            return;
        }

        if (12 + 32 != dataLength) { //4 acount type + 4 key id + 4 account number + 32 auth key
            G_io_apdu_buffer[(*tx)++] = RET_VAL_BAD_SIZE_ERR;
            return;
        }

        uint8_t outputSize = 0;
        uint16_t exception = 0;

        G_io_apdu_buffer[(*tx)++] = RET_VAL_SUCCESS;

        uint8_t ret = signHash(dataBuffer + 32, G_io_apdu_buffer + 1, &outputSize, &exception);

        if (0 == ret) {
            *tx += outputSize;
        } else {
            *tx -= 1;

            if  (1 == ret) {
                G_io_apdu_buffer[(*tx)++] = RET_VAL_KEY_DERIVE_EXCEPTION;
            } else {
                G_io_apdu_buffer[(*tx)++] = RET_VAL_SIGN_EXCEPTION;
            }

            G_io_apdu_buffer[(*tx)++] = exception >> 8;
            G_io_apdu_buffer[(*tx)++] = exception & 0xFF;
        }

    } else {
        G_io_apdu_buffer[(*tx)++] = RET_VAL_BAD_CMD_PARAM_ERR;
    }
    
    return;
}

//This handler calls the helper and sticks 0x9000 at the end, its a sort of EVERYTHING IS OK indicator
void authTxnHashHandler(uint8_t p1, uint8_t p2, uint8_t *dataBuffer, uint16_t dataLength,
                volatile unsigned int *flags, volatile unsigned int *tx) {

    authTxnHashHandlerHelper(p1, p2, dataBuffer, dataLength, flags, tx);

    if (0 == ((*flags) & IO_ASYNCH_REPLY)) {
        G_io_apdu_buffer[(*tx)++] = 0x90;
        G_io_apdu_buffer[(*tx)++] = 0x00;
    }
}


