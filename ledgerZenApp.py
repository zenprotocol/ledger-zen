from ledgerblue.comm import getDongle
from ledgerblue.commException import CommException
import argparse
import struct
from decimal import Decimal
import hashlib
import base58
import bech32
import sha3
import random
import time
import binascii

class ledgerZenApp:

    CODE_LENGTH = 5
    KEY_LENGTH = 32

    def __init__(self):
        #note: if your implementing this in some other place, you might want to use a secure source of entropy
        self.changeKey()
        self.doneAuth = False
        self.dongle = getDongle(True)

    def changeKey(self):
        self.key = bytearray(chr(random.getrandbits(8)) for i in xrange(self.KEY_LENGTH))

    #The null terminater param is used for testing, for all other purposes it should be 0
    def doAuth(self, code, nullTerminator = 0):

        if len(code) != self.CODE_LENGTH:
            raise Exception("Code length is {} instead of {}".format(len(code), self.CODE_LENGTH))

        apdu = "e0020000".decode('hex') + chr(self.CODE_LENGTH + self.KEY_LENGTH + 1) + code + chr(nullTerminator) + self.key

        result = self.dongle.exchange(bytes(apdu))

        return 0 == result[0]

    def doHexSha(self, inp): #sha3_256 ret is in hex
        h = sha3.sha3_256()
        h.update(inp)
        return h.digest()

    def doSha(self, inp):
        return list(ord(x) for x in self.doHexSha(inp))

    def getPublicKeys(self, walletStartIndex, coinType, accountNum):
        # note: start index is multiplied by the number of keys sent back, which is currently 7

        apdu = "e0030000".decode('hex') + chr(self.KEY_LENGTH + 12) + self.key + bytearray(struct.pack("<I", walletStartIndex) + struct.pack("<I", coinType) + struct.pack("<I", accountNum))
        result = self.dongle.exchange(bytes(apdu))

        if result[0] != 0:
            raise Exception("got a no go for get public keys, got : {}".format(result[0]))

        result = result[1:]

        return list([str(result[i * 33:(i + 1) * 33]).encode('hex'), self.doHexSha(result[i * 33:(i + 1) * 33]).encode('hex'),
                      bech32.encode("zen", 0, self.doSha(result[i * 33:(i + 1) * 33]))] for i in xrange(0, 7))

    #returns the version and app code
    def getVersion(self, numOfTimes):

        t0 = time.time()

        for i in xrange(numOfTimes):
            apdu = "e001".decode('hex') + chr(1) + chr(2) + chr(self.KEY_LENGTH) + self.key + '1' * 200
            result = self.dongle.exchange(bytes(apdu))

        return result[0], result[1:]


    def signTxn(self, keyNum, coinType, accountNum):

        apdu = "e0040200".decode('hex') + chr(32 + 12) + self.key + bytearray(struct.pack("<I", keyNum) + struct.pack("<I", coinType) + struct.pack("<I", accountNum))
        result = self.dongle.exchange(bytes(apdu))
        if 0 != result[0]:
            print("got {}, instead of success...".format(result[0]))
            return result[0]

        if result[4] not in (32, 33):
            print("got weird s length {}".format(result[4]))
            return -1

        startPos = 5 + (1 if 33 == result[4] else 0)
        s = str(result[startPos:startPos + 32]).encode('hex')

        rLength = result[startPos + 32 + 1]
        if rLength not in (32, 33):
            print("got weird r length {}".format(rLength))
            return -1

        startPos = startPos + 32 + 2 + (1 if 33 == result[startPos + 32 + 1] else 0)
        r = str(result[startPos:startPos + 32]).encode('hex')

        return 0, {'s': s, 'r': r}


    #without init is only used for testing, for regular use, set it to False
    def calcTxHash(self, txn, debugCase, sendLength, withoutInit = False):


        prefixLength = 5 + 32
        maxLength = prefixLength + sendLength#254 #Not 255 for safty, maybe this should be changed later

        if maxLength > 254:
            return "Error, send length: {} exceding limit (254) ".format(maxLength)

        if withoutInit:
            i = 1
        else:
            i = 0

        sendOneEmpty = False
        while (i < len(txn)) or sendOneEmpty:
            
            if 0 == i:
                additionalByte = chr(0)
            else:
                additionalByte = chr(1)

            if sendOneEmpty:
                print("sending one empty")
                apdu = "e004".decode('hex') + additionalByte + chr(debugCase) + chr(len(self.key)) + self.key
            else:
                amountToSend = min(len(txn) - i, maxLength - prefixLength)
                apdu = "e004".decode('hex') + additionalByte + chr(debugCase) + chr(len(self.key) + amountToSend) + self.key + txn[i:i+amountToSend]
                i += amountToSend
                print("Sending {} from point:{} out of {}".format(amountToSend, i - amountToSend, len(txn)))

            sendOneEmpty = False
            result = self.dongle.exchange(bytes(apdu))

            if 0 == result[0]:

                if len(txn) == i: #Everything was sent
                    if 19 != result[1]:
                        print("Error, sent all the file but didn't get FINISHED got {} instead, thats weird".format(result[1]))
                        return False
                    
                    if 2 + 32 != len(result):
                        print("Got done, but wrong length, got {}".format(len(result)))
                        return False

                    return result[2:]

                if 2 != len(result):
                    print("Didn't get extention value for 0 return code, weird")
                    return False

                if 19 == result[1]:
                    print("Error: Got finished message although the whole buffer isn't sent")
                    return False

                elif 10 == result[1]:
                    print("Got continue command")
                    continue

                print("Error: Got an error msg while parsing {}".format(result[1]))
                return False

            elif 1 == result[0]:
                print("Sending empty msg to continue parsing")
                sendOneEmpty = True
                continue

            elif 2 == result[0]:
                print("got output unapproved")
                return False
            else:
                print("got some sort of error, first value is: {}".format(result[0]))
                return False
